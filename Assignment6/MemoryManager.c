//=============================================================================
// Name        : MemoryManager.c
// Author      : Kelly E Lamb
// Date        : 2020/05/17
// Professor   : Michael Landreth
// Class       : CST-221-O500 - Operating System Concepts
// Assignment  : 6 - Memory Manager
// Description :
//              Program to request user input integer 0 - 4095
//              Display as binary, hexadecimal, then transform it
//              and display as decimal, binary, hexadecimal
//=============================================================================

#include <stdio.h>
#include <string.h>

//
// Prompt user to enter value between low and high
//
unsigned int getValue(unsigned int low, unsigned int high)
{
	unsigned int result;

	do
	{
		printf("Please enter a number between %d and %d: ", low, high);
		scanf("%d", &result);
	} while (result < low || result > high);

	return result;
}

//
// Display number converted to binary
//
void displayBinary(unsigned int number)
{
	unsigned int mask = 1<<31;

	printf("Converted to Binary     : ");

	for (int hbyte = 0; hbyte < 8; hbyte++)
	{
		for (int bit = 0; bit < 4; bit++)
		{
			char c = (number & mask) == 0 ? '0' : '1';
			putchar(c);
			mask >>= 1;
		}
		putchar(' ');
	}
	putchar('\n');
	return;
}

//
// Display number converted to hexadecimal
//
void displayHexadecimal(unsigned int number)
{
	printf("Converted to Hexadecimal: %04x\n", number);
	return;
}

//
// Display number as decimal
//
void displayDecimal(unsigned int number)
{
	printf("Transformed Decimal     : %d\n", number);
	return;
}

//
// Transform number per assignment instructions
// shift number 16 bits left, mask out bottom 16 bits
// and or number with 0x07FF
//
void transformNumber(unsigned int *number)
{
	unsigned int result = *number << 16;
	result = result & 0xFFFF0000; // mask out bottom 16 bits
	result = result | 0x07FF;
	*number = result;
	return;
}

//
// Main Program
//
int main (void)
{
	unsigned int value = getValue(0, 4095);

	displayBinary(value);
	displayHexadecimal(value);

	transformNumber(&value);

	displayDecimal(value);
	displayBinary(value);
	displayHexadecimal(value);

	return 0;
}

