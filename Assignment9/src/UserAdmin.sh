#!/bin/bash
# =============================================================================
#  Name        : UserAdmin.sh
#  Author      : Kelly E Lamb
#  Date        : 2020/06/09
#  Professor   : Michael Landreth
#  Class       : CST-221-O500-Operating System Concepts
#  Assignment  : 9 - Security
#  Description :
#                Requires 3 parameters - userfile, usergroup, operation(-a/-r)
#                userfile  - username and encrypted password. space delimited
#                usergroup - group to add new users - will created if needed
#                operation - -a = add, -r = remove. (will add/remove home dir)
#                Requires superuser access to run.
#  Objective   :
#                Manage user administration
#                Applies to Ubuntu syntax
# =============================================================================

#
# Validate process running as superuser.
#
if [ $(id -u) -ne 0 ]; then
    echo
    echo "You must run process as superuser."
    echo
    exit 1
fi

#
# Validate argument count
#
if [ "$#" -ne 3 ]; then
    echo
    echo "You must supply 3 arguments (userfile, usergroup, operation -a/-r)"
    echo
    exit 1
fi

#
# Set up variables to arguments
#
userfile=$1
group=$2
operation=$3

#
# Add group if needed
#
if grep -q $group /etc/group
then
    echo "Group $group exists!"
else
    groupadd $group
    [ $? -eq 0 ] && echo "Group add successful." || echo "*** Failed to add group. ***"
fi

#
# Handle User Addition - loop through userfile
# Ignore blank lines in file via sed
#
if [[ "$operation" == "-a" ]]; then

    sed '/^[ \t]*$/d' $userfile | while read -r name pass
    do
        #
        # Assert name is a new user
        #
        egrep "^$name" /etc/passwd >/dev/null
        if [ $? -eq 0 ]; then
            echo "$name exists!"
            exit 1
        else
            adduser $name --disabled-password --gecos "" --ingroup $group
            [ $? -eq 0 ] && echo "User add successful." || echo "*** Failed to add user. ***"

            usermod $name -p $pass
            [ $? -eq 0 ] && echo "User password successful." || echo "*** Failed to modify user password. ***"
            echo
        fi
    done
fi

#
# Handle User Removal - loop through userfile
# Ignore blank lines in file via sed
#
if [[ "$operation" == "-r" ]]; then

    sed '/^[ \t]*$/d' $userfile | while read -r name pass
    do
        deluser $name --remove-home
        echo
    done

    if grep -q $group /etc/group
    then
        delgroup $group --only-if-empty
    else
        echo "*** Group does not exist. ***"
    fi

fi

exit 0


