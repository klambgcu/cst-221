#!/bin/bash
# =============================================================================
#  Name        : PassStrength.sh
#  Author      : Kelly E Lamb
#  Date        : 2020/06/09
#  Professor   : Michael Landreth
#  Class       : CST-221-O500-Operating System Concepts
#  Assignment  : 9 - Security
#  Description :
#                Requires 1 argument password
#                Validates strength of password upon the following criteria
#                1. Minimum length of 8 characters
#                2. Must contain at least one numeric character
#                3. Must contain at least one of the following non-alphabetic
#                   characters: @, #, $, %, &, *, +, -, =
#  Objective   :
#                Verify password strength - report/flag weak passwords
#                Applies to Ubuntu syntax
# =============================================================================

#
# Validate argument count
#
if [ "$#" -ne 1 ]; then
    echo
    echo "You must supply 1 argument (password)"
    echo
    exit 1
fi

#
# Password strength indicator
#
strength=0
password=$1

#
# Test minimum length
#
passwordLength=${#password}
if [[ "$passwordLength" -lt "8" ]]; then
    echo "Must have minimum length of 8 characters"
else
    let "strength++"
fi

#
# Test for numeric character
#
if [[ $password =~ [0-9] ]]; then
    let "strength++"
else
    echo "Must contain at least one numeric character"
fi

#
# Test for special characters @, #, $, %, &, *, +, -, =
#
if [[ $password == *[@#\$%\&\*\+\-\=]* ]]; then
    let "strength++"
else
     echo "Must contain at least one: @, #, $, %, &, *, +, -, ="
fi

if [ "$strength" -eq "3" ]; then
        echo
        echo "Password ("$password") is secure!"
        echo
    else
        echo
        echo "Password ("$password") is weak and insecure!"
        echo
fi

