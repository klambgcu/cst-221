//=============================================================================
// Name        : DeadlockAvoidance.c
// Author      : Kelly E Lamb
// Date        : 2020/05/16
// Professor   : Michael Landreth
// Class       : CST-221-O500 - Operating System Concepts
// Assignment  : 5 - Deadlock Avoidance
// Description :
//              Create some threads that call a function that tempts deadlock
//              issue. Supply timeout of one second per iteration in function
//              to avoid deadlock issue. Introduce another mutex to see if a
//              another process will wake up and try to grab resource.
//=============================================================================

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

//
// Create mutex object.
//
pthread_mutex_t mutex1;
pthread_mutex_t mutex2;

//
// Define file pointer for activity log
//
FILE *fp;

//
// A simple function to test deadlock avoidance.
//
void *avoid_deadlock (void *label)
{
	char *name;
	name = (char *) label;

	struct timespec timeoutTime;
	clock_gettime(CLOCK_REALTIME, &timeoutTime);
	timeoutTime.tv_sec += 1;

	//
	// Attempt to lock mutex
	//
	if (pthread_mutex_timedlock(&mutex1, &timeoutTime) == 0)
	{
		fprintf(fp, "%s has lock mutex1 \n", name);

		//
		// Add 2nd mutex to see if a dfferent process will wake
		// up and try to grab lock too early
		//
		if (pthread_mutex_timedlock(&mutex2, &timeoutTime) == 0)
		{
			fprintf(fp, "%s has lock mutex2 \n", name);

			for (int count = 0; count < 5; count++)
			{
				//
				// Print Info
				//
				fprintf(fp, "%s = %d\n", name, count);
			}
		}
		pthread_mutex_unlock(&mutex2);
		pthread_mutex_unlock(&mutex1);
	}

	return NULL;
}

int main(int argc, char *argv[])
{
	//
	// Define test variables
	//
	pthread_t thread1;
	pthread_t thread2;
	pthread_t thread3;

	char *name1 = "Thread 1";
	char *name2 = "Thread 2";
	char *name3 = "Thread 3";

	int result1;
	int result2;
	int result3;

	//
	// Open activity Log - print header information
	//
	fp = fopen("activity.log", "w");

	fprintf(fp, "Activity Log:\n");
	fprintf(fp, "Results from thread execution:\n");

	//
	// Create three threads/processes to test deadlock avoidance function
	//
	result1 = pthread_create(&thread1, NULL, avoid_deadlock, (void *) name1);
	result2 = pthread_create(&thread2, NULL, avoid_deadlock, (void *) name2);
	result3 = pthread_create(&thread3, NULL, avoid_deadlock, (void *) name3);

	//
	// Join threads for communication with main thread
	//
	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL);
	pthread_join(thread3, NULL);

	//
	// Display results
	//
	fprintf(fp, "Thread 1 Results: %d\n", result1);
	fprintf(fp, "Thread 2 Results: %d\n", result2);
	fprintf(fp, "Thread 3 Results: %d\n", result3);

	fclose(fp); // Always close files

	return 0;
}




