//=============================================================================
// Name        : shoe_semaphore.c
// Author      : Kelly E Lamb
// Date        : 2020/05/10
// Professor   : Michael Landreth
// Class       : CST-221-O500 - Operating System Concepts
// Assignment  : 4 - Semaphore for selling shoes
// Description :
//              Create a process that allows multiple salesmen to sell shoes
//              Synchronization is necessary since we cannot sell more shoes
//              we have in stock or put on the rack.
//              In this instance, we have a total of 20 shoes for sell but
//              we have to make sure our awesome salesmen do not sell too much.
//=============================================================================

#include<pthread.h>
#include<semaphore.h>
#include<stdio.h>
#include<sys/stat.h>
#include<sys/types.h>

#define MAX_SHOES 20

sem_t sem_id;
int num_of_shoes = MAX_SHOES;
int salesmen = 2; // Pair of Al Bundy :)

// Process to sell shoes
void *ShoeSales()
{
	while(1)
	{
		int ret;
		ret = sem_wait(&sem_id);
		if(num_of_shoes > 0)
		{
			num_of_shoes--;
			printf("Shoe Pair item %i purchased. Number remaining: %i\n", (MAX_SHOES - num_of_shoes), num_of_shoes);
		}
		else
		{
			ret = sem_post(&sem_id);
			break;
		}
		ret = sem_post(&sem_id);
	}
	pthread_exit(NULL);
}

int main()
{
	int ret;
	pthread_t thread_id[salesmen];

	// Semaphore initilization
	ret = sem_init(&sem_id, 1 ,1);

	// Create Salesmen threads
	for (int i = 0; i < salesmen; i++)
	{
		ret = pthread_create(&thread_id[i], NULL, ShoeSales, NULL);
	}

	// Join threads for communication
	for(int j = 0; j < salesmen; j++)
	{
		pthread_join(thread_id[j], NULL);
	}
}
