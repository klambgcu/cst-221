//=============================================================================
// Name        : shoe_monitor.c
// Author      : Kelly E Lamb
// Date        : 2020/05/10
// Professor   : Michael Landreth
// Class       : CST-221-O500 - Operating System Concepts
// Assignment  : 4 - Monitor for selling shoes
// Description :
//              Create a process that produces shoes for sell.
//              Using a Producer/Consumer strategy, we create the shoes and
//              place them on the rack for sale. Then consumer comes by and
//              purchases them. In this instance, we will create 20 shoes for
//              sell but we have to make sure more than we create. This creates
//              the need to synchronize the actions.
//=============================================================================

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MAX_SHOES 20

pthread_mutex_t mutex;
pthread_cond_t cond_consumer, cond_producer;
int rack = 0;

// Produce some shoes - 1 at a time
void *producer(void *ptr)
{
	for (int i = 1; i <= MAX_SHOES; i++)
	{
		pthread_mutex_lock(&mutex);
		while(rack != 0)
		{
			pthread_cond_wait(&cond_producer, &mutex);
		}
		rack = i; // store pair of shoes on the rack
		pthread_cond_signal(&cond_consumer);
		pthread_mutex_unlock(&mutex);
		printf("Shoe Pair item %i added to rack.\n", i);
	}
	pthread_exit(0);
}

// Purchase pair of shoes - 1 at a time
void *consumer(void *ptr)
{
	for (int i = 1; i <= MAX_SHOES; i++)
	{
		pthread_mutex_lock(&mutex);
		while(rack == 0)
		{
			pthread_cond_wait(&cond_consumer,&mutex);
		}
		printf("Shoe Pair Item %i purchased.\n", rack);
		rack = 0;
		pthread_cond_signal(&cond_producer);
		pthread_mutex_unlock(&mutex);
	}
	pthread_exit(0);
}

int main (int argc, char **argv)
{
	// Define and initialize threads for Producer/Consumer
	pthread_t pro, con;

	// Define and initialize mutex
	pthread_mutex_init(&mutex, 0);

	// Create conditions
	pthread_cond_init(&cond_consumer,0);
	pthread_cond_init(&cond_producer,0);

	// Create threads
	pthread_create(&con, 0, consumer, 0);
	pthread_create(&pro, 0, producer, 0);

	// Join threads for communication
	pthread_join(pro,0);
	pthread_join(con, 0);

	// Do some clean up
	pthread_cond_destroy(&cond_consumer);
	pthread_cond_destroy(&cond_producer);

	pthread_mutex_destroy(&mutex);
}